var http = require('http');
var url = require('url');
var weibo = require('./auth.js').Weibo;

http.createServer(function(req, res){
    var pathname = url.parse(req.url).pathname;
    var wbInit = {
        'client_id' : 'your client id',
        'client_secret' : 'your client secret',
        'redirect_uri' : 'your redirect uri'
    }

    var wb = new weibo(wbInit);
    wb.oAuth2(req, res, function(authInfo){
        //do something
    });
}).listen(8123);

console.log('server is running on port', 8123);