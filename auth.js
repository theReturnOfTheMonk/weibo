var https = require('https');
var url = require('url');
var path = require('path');
var fs = require('fs');
var querystring = require('querystring');
var multiparter = require("multiparter");
var apiList = require('./api_list.js').apiList;

var Weibo = exports.Weibo = function(init){
    this.client_id = init.client_id;
    this.client_secret = init.client_secret;
    this.redirect_uri = init.redirect_uri;
    this.response_type = 'code';
    this.POSTPORT = init.POSTPORT || 443;
}

Weibo.prototype = {
    constructor : Weibo,
    //生成授权地址
    getAuthorizeURL : function(){
        var opts = '?' + querystring.stringify({
            'client_id' : this.client_id,
            'redirect_uri' : this.redirect_uri,
            'response_type' : this.response_type
        });
        return apiList.oAuth2.authorize + opts;
    },
    //授权
    oAuth2 : function(req, res, callback){
        var that = this;
        var authorizeURL = this.getAuthorizeURL();
        var parsedURL = url.parse(req.url, true);

        if(parsedURL.pathname == '/login') {
            res.writeHeader(302, {'location' : authorizeURL, 'port' : 443});
            res.end();
        }

        if(parsedURL.query && parsedURL.query.code) {
            this.code = parsedURL.query.code;
            this.getAccessToken(callback);
            res.writeHeader(302, {'location' : '/home'});
            res.end();
        }
    },
    //获取access token
    getAccessToken : function(callback){
        var that = this;
        var postData = querystring.stringify({
            "client_id" : this.client_id,
            "client_secret" : this.client_secret,
            "grant_type" : "authorization_code",
            "redirect_uri" : this.redirect_uri,
            "code" : this.code
        });

        var parsedURL = url.parse(apiList.oAuth2.access_token);
        var opts = {
            host : parsedURL.hostname,
            port : this.POSTPORT,
            path : parsedURL.path,
            method : "POST"
        }

        this._post(postData, opts, function(data){
            var accessTokenObj = JSON.parse(data);
            that.accessToken = accessTokenObj.access_token;
            that.remindIn = accessTokenObj.remind_in;
            that.expiredIn = accessTokenObj.expired_in;
            that.uid = accessTokenObj.uid;

            callback(that);
        });
    },
    //私有POST方法, 所有参数为必选项
    _post : function(postData, opts, callback){
        var that = this;
        //上传文本: multipart/form-data
        //普通post: application/x-www-form-urlencoded
        if(!opts.headers) {
            opts.headers = {'Content-Type' : 'application/x-www-form-urlencoded'};
        }

        //HTML头的"Content-Length"需要的是字节数, 不是字符的个数, 所以用byteLength
        if (Buffer.isBuffer(postData)) {
            opts.headers['Content-Length'] = postData ? postData.length: 0;
        } else {
            opts.headers['Content-Length'] = postData ? Buffer.byteLength(postData) : 0;
        }

        var request = https.request(opts, function(response){
            var responseData = '';
            response.on('data', function(chunk){
                responseData += chunk;
            });

            response.on('end', function(){
                callback(responseData);
            });

            response.on('error', function(e){
                console.log('response error', e);
            }); 
        });

        if(postData) {
            request.write(postData);
        }

        request.on('error', function(e){
            console.log('request error', e)
        });

        request.end();
    },
    //私有GET方法
    _get : function(requestURL, callback){
        var httpsReq = https.get(requestURL, function(httpsRes) {
            var responseData = '';
            httpsRes.setEncoding ('utf-8');
            httpsRes.on("data", function(chunk){
                responseData += chunk;
            });
            httpsRes.on("end", function(){
                callback(responseData);
            });
            httpsRes.on("error", function (e) {
                console.log('error:', e);
            });
        });
        httpsReq.on('error', function(e){
            console.log('get error:', e);
        });
    },
    //根据用户ID获取用户信息
    show : function(paramObj, callback){
        var query = {
            'source' : this.client_id,
            'access_token' : this.accessToken,
            'uid' : this.uid
        }

        if(paramObj.uid && paramObj.uid != this.uid) {
            query.uid = paramObj.uid;
        }

        if(paramObj.screenName) {
            delete query.uid;
            query.screen_name = paramObj.screenName;
        }

        var requestURLquery = '?' + querystring.stringify(query);
        var requestURL = apiList.users.shows + requestURLquery;

        this._get(requestURL, function(responseData){
            callback(JSON.parse(responseData));
        });
    },
    //获取用户发布的微博的ID
    ids : function(paramObj, callback){
        var query = {
            'source' : this.client_id,
            'access_token' : this.accessToken,
            'uid' : this.uid
        }

        if(paramObj.uid && paramObj.uid != this.uid) {
            query.uid = paramObj.uid;
        }

        if(paramObj.screenName) {
            delete query.uid;
            query.screen_name = paramObj.screenName;
        }

        var requestURLquery = '?' + querystring.stringify(query);
        var requestURL = apiList.statuses.ids + requestURLquery;

        this._get(requestURL, function(responseData){
            callback(JSON.parse(responseData));
        });
    },
    //发布一条新微博
    update : function(postData, callback){
        var that = this;
        var _postData = querystring.stringify({
            "source" : this.client_id,
            "access_token" : this.accessToken,
            'status' : postData.content
        });

        var parsedURL = url.parse(apiList.statuses.update);
        var opts = {
            host : parsedURL.hostname,
            port : this.POSTPORT,
            path : parsedURL.path,
            method : "POST"
        }
        that._post(_postData, opts, function(responseData){
            callback(JSON.parse(responseData));
        });
    },
    //发送一条带图微博
    //传图这个是抄的... 忘了出处了- -..
    upload: function (postData, callback) {
        var that = this;
        var requestParms = {
            status : 'status',
            pic : 'pic'
        }
        var IMG_CONTENT_TYPES = {
            '.gif': 'image/gif',
            '.jpeg': 'image/jpeg',
            '.jpg': 'image/jpeg',
            '.png': 'image/png'
        }

        var parsedURL = url.parse(apiList.statuses.upload);
        var request = new multiparter.request(https, {
            host: parsedURL.hostname,
            port: '443',
            path: parsedURL.path,
            method: "POST",
            headers: { //上传文件时使用Authorization header做授权认证
                'Authorization': 'OAuth2 ' + that.accessToken
            }
        });

        request.setParam(requestParms.status, postData.content);
        fs.stat(postData.uploadFile, function (err, stats) {
            if (err) {
                throw err;
            }
            request.addStream(
                requestParms.pic,
                path.basename(postData.uploadFile),
                IMG_CONTENT_TYPES[path.extname(postData.uploadFile)],
                stats.size,
                fs.createReadStream(postData.uploadFile)
            );
            request.send(function (error, res) {
                if (error) {
                    console.log(error);
                }
                var list = [];
                res.on('data', function (chunk) {
                    list.push(chunk);
                });
                res.on('end', function () {
                    callback(JSON.parse(Buffer.concat(list).toString()));
                    list = null;
                });

                res.on("error", function (error) {
                    console.log('upload image error:', error);
                });
            });
        });
    },
    //指定一个图片URL地址抓取后上传并同时发布一条新微博
    //没权限.. 日..
    upload_url_text : function(postData, callback){
        var that = this;
        var _postData = querystring.stringify({
            "source" : this.client_id,
            "access_token" : this.accessToken,
            'status' : postData.content,
            'url' : postData.uploadFile
        });

        var parsedURL = url.parse(apiList.statuses.upload_url_text);
        var opts = {
            host : parsedURL.hostname,
            port : this.POSTPORT,
            path : parsedURL.path,
            method : "POST"
        }
        that._post(_postData, opts, function(responseData){
            callback(JSON.parse(responseData));
        });
    },
    //获取用户的关注列表 
    friends : function (callback){
        var requestURLquery = '?' + querystring.stringify({
            "source" : this.client_id,
            "access_token" : this.accessToken,
            "uid" : this.uid
        });
        var requestURL = apiList.friendships.friends + requestURLquery;
        this._get(requestURL, function(responseData){
            callback(JSON.parse(responseData));
        });
    },
    //获取用户发布的微博
    user_timeline : function(paramObj, callback){
        var that = this;
        var query = {
            "source" : this.client_id,
            "access_token" : this.accessToken,
            "uid" : this.uid,
        }
        if(paramObj.count) {
            query.count = paramObj.count;
        }


        if(paramObj.uid && paramObj.uid != this.uid) {
            query.uid = paramObj.uid;
        }

        if(paramObj.screenName) {
            delete query.uid;
            query.screen_name = paramObj.screenName;
        }

        var requestURLquery = '?' + querystring.stringify(query);
        var requestURL = apiList.statuses.user_timeline + requestURLquery;
        this._get(requestURL, function(responseData){
            callback(JSON.parse(responseData));
        });
    },
    //评论一条微博
    create : function(paramObj, callback){
        var that = this;
        var _postData = querystring.stringify({
            "source" : this.client_id,
            "access_token" : this.accessToken,
            'comment' : paramObj.comment,
            'id' : paramObj.id
        });

        var parsedURL = url.parse(apiList.comments.create);
        var opts = {
            host : parsedURL.hostname,
            port : this.POSTPORT,
            path : parsedURL.path,
            method : "POST"
        }
        that._post(_postData, opts, function(responseData){
            callback(JSON.parse(responseData));
        });
    }
}