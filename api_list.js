var apiList = exports.apiList = {
    'oAuth2' : {
        'authorize' : 'https://api.weibo.com/oauth2/authorize',
        'access_token' : 'https://api.weibo.com/oauth2/access_token'
    },
    'users' : {
        //获取用户信息
        'shows' : 'https://api.weibo.com/2/users/show.json',
    },
    'statuses' : {
        //获取用户发布的微博 
        'user_timeline' : 'https://api.weibo.com/2/statuses/user_timeline.json',
        //获取用户发布的微博的ID
        'ids' : 'https://api.weibo.com/2/statuses/user_timeline/ids.json',
        //发布一条微博信息
        'update' : 'https://api.weibo.com/2/statuses/update.json',
        //上传图片并发布一条新微博
        'upload' : 'https://upload.api.weibo.com/2/statuses/upload.json',
        //指定一个图片URL地址抓取后上传并同时发布一条新微博(没权限)
        'upload_url_text' : 'https://api.weibo.com/2/statuses/upload_url_text.json'
    },
    'friendships' : {
        //获取用户的关注列表
        'friends' : 'https://api.weibo.com/2/friendships/friends.json'
    },
    'comments' : {
        //评论一条微博
        'create' : 'https://api.weibo.com/2/comments/create.json'
    }
}
